import { React, useState } from 'react';
import { v4 as uuidv4 } from 'uuid';

// Style
import './ToDoWrapper.scss';
import ToDoForm from '../ToDoForm';
import ToDo from '../ToDo/ToDo';
import EditToDoForm from '../EditToDoForm';

uuidv4();

const ToDoWrapper = () => {
  const [todos, setTodos] = useState([]);

  const addTodo = (todo) => {
    setTodos([...todos, {
      id: uuidv4(), task: todo, completed: false, isEditing: false,
    }]);
  };

  const toggleComplete = (id) => {
    setTodos(todos.map((todo) => (todo.id === id
      ? { ...todo, completed: !todo.completed } : todo)));
  };

  const deleteToDo = (id) => {
    setTodos(todos.filter((todo) => todo.id !== id));
  };

  const editToDo = (id) => {
    setTodos(todos.map((todo) => (todo.id === id
      ? { ...todo, isEditing: !todo.isEditing } : todo)));
  };

  const editTask = (task, id) => {
    setTodos(todos.map((todo) => (todo.id === id
      ? { ...todo, task, isEditing: !todo.isEditing } : todo)));
  };

  return (
    <div className="c-todo-wrapper">
      <h1>Get Things Done!</h1>
      <ToDoForm addTodo={addTodo} />
      {todos.map((todo, index) => (
        todo.isEditing ? (
          <EditToDoForm
            editTask={editTask}
            task={todo}
          />
        ) : (
          <ToDo
            task={todo}
            index={index}
            toggleComplete={toggleComplete}
            deleteToDo={deleteToDo}
            editToDo={editToDo}
          />
        )
      ))}
    </div>
  );
};

export default ToDoWrapper;
