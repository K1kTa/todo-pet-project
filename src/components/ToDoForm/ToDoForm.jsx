import {
  React,
  useState,
  useRef,
  useEffect,
} from 'react';

// Style
import './ToDoForm.scss';

const ToDoForm = ({ addTodo }) => {
  const [inputValue, setInputValue] = useState('');

  const refForInput = useRef();

  const handleSubmit = (event) => {
    event.preventDefault();
    if (inputValue !== '') {
      addTodo(inputValue);
    }

    setInputValue('');
  };

  useEffect(() => {
    refForInput.current.focus();
  }, []);

  return (
    <form className="c-todo-form" onSubmit={handleSubmit}>
      <input
        ref={refForInput}
        type="text"
        placeholder="What is the task today?"
        className="c-todo-form__input"
        onChange={(event) => { setInputValue(event.target.value); }}
        value={inputValue}
      />
      <button type="submit" className="c-todo-form__button">
        Add Task
      </button>
    </form>
  );
};

export default ToDoForm;
