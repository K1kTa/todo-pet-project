import React from 'react';
import './ToDoHome.scss';
import ToDoWrapper from '../ToDoWrapper';

const ToDoHome = () => (
  <div className="c-todo-home">
    <ToDoWrapper />
  </div>
);

export default ToDoHome;
