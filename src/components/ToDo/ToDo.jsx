import { React } from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faPenToSquare, faTrash } from '@fortawesome/free-solid-svg-icons';

// Style
import './ToDo.scss';

const ToDo = (props) => {
  const {
    task,
    toggleComplete,
    deleteToDo,
    editToDo,
  } = props;
  return (
    <div className="c-todo">
      <p
        className={`${task.completed ? 'c-todo__completed' : 'c-todo__incompleted'}`}
        onClick={() => toggleComplete(task.id)}
      >
        {task.task}
      </p>
      <div>
        <FontAwesomeIcon icon={faPenToSquare} onClick={() => editToDo(task.id)} />
        <FontAwesomeIcon icon={faTrash} onClick={() => deleteToDo(task.id)} />
      </div>
    </div>
  );
};

export default ToDo;
