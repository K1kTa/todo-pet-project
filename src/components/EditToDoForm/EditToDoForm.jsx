import {
  React,
  useState,
  useRef,
  useEffect,
} from 'react';

// Style
import './EditToDoForm.scss';

const EditToDoForm = (props) => {
  const { editTask, task } = props;

  const updateInputRef = useRef(null);

  const [inputValue, setInputValue] = useState(task.task);

  const handleSubmit = (event) => {
    event.preventDefault();
    editTask(inputValue, task.id);

    setInputValue('');
  };

  useEffect(() => {
    updateInputRef.current.focus();
  }, []);

  return (
    <form className="c-edit-todo-form" onSubmit={handleSubmit}>
      <input
        ref={updateInputRef}
        type="text"
        placeholder="Update Task"
        className="c-edit-todo-form__input"
        onChange={(event) => { setInputValue(event.target.value); }}
        value={inputValue}
      />
      <button type="submit" className="c-edit-todo-form__button">
        Update Task
      </button>
    </form>
  );
};

export default EditToDoForm;
