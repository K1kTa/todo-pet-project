import React from 'react';
import ReactDOM from 'react-dom/client';
import ToDoHome from './components/ToDoHome/ToDoHome';
import reportWebVitals from './reportWebVitals';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    <ToDoHome />
  </React.StrictMode>,
);

reportWebVitals();
